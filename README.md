# CS_lab10
SSL Pinning
Probably the most well known attack in cybersecurity is the Man-in-the-middle (MITM) attack.
A mechanism that helps combat such attacks is called SSL pinning. The task for this week’s
laboratory work is to create a native mobile application which would be protected from MITM
attacks via SSL pinning.
For starters, you will need to create a mobile app that uses a 3’rd party platform for
authentication. Following, you’ll implement SSL pinning to protect your application from
MITM attacks. After applying the defence mechanism, you’ll need to show its effectiveness via
a video. To summarize, you need to:
• Create a native mobile application which performs authentication via a 3’rd party;
• Implement SSL pinning to combat MITM attacks;
• Prove the effectiveness of the implemented mechanism (via video).

# Functionality
Sign In Using Firebase In Android Studio

To run the project is needed:
- 1.Add the project to Firebase console:
- https://console.firebase.google.com/u/0/
- 2.Authenticate Using Google Sign-In on Android:
- https://firebase.google.com/docs/auth/android/google-signin
- 3.Download 'google-services.json file' from Firebase Project Settings and add it to the root Directory of the project.
